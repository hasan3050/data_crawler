//var client = require('../connection').cassandraClient;
var twitter = require('./helper/twitter');
var disqus = require('./helper/disqus');
var async = require('async');
var fs=require('fs');
var config = require('../config');

module.exports ={
	'disqusTimeline': function(req,res){
		disqus.timeline({
			username	: (req.query.username ? req.query.username : 'reinout'),
			postLimit 	: ((req.query.postLimit || req.query.postLimit == 0)? parseInt(req.query.postLimit) : 2000),
			cursor 		: (req.query.cursor ? req.query.cursor : '')
		}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'post saveDisqusTimeline': function(req,res){
		disqus.saveTimeline({
			userid		: (req.body.userid ? req.body.userid : '18'),
			username	: (req.body.username ? req.body.username : 'reinout'),
			postLimit 	: ((req.body.postLimit || req.body.postLimit == 0)? parseInt(req.body.postLimit) : 2000),
			cursor 		: (req.body.cursor ? req.body.cursor : '')
		}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'post saveDisqusTimelineBatch': function(req,res){
		var offset = ((req.body.offset)? parseInt(req.body.offset) : 0);
		var limit  = ((req.body.limit || req.body.limit == 0)? parseInt(req.body.limit) : 100);
		var filename = (req.body.filename ? req.body.filename : config.data.disqusUserWithProfiles.base+'/merged/1-200000.json');
		var postLimit = ((req.body.postLimit || req.body.postLimit == 0)? parseInt(req.body.postLimit) : 2000);
			
		var userProfileFile = JSON.parse(fs.readFileSync(filename, 'utf-8'));
		var userIds = Object.keys(userProfileFile);
		var candidateUserInfo = [];

		var foldernames=fs.readdirSync(config.data.media.base);
		var folderMap = {}
		for(var f=0; foldernames[f];f+=1){
			var id = foldernames[f].split('_')[0];
			folderMap[id] = 'done'
		}

		for(var index = offset; index < offset+limit && userIds[index]; index+=1){
			var userid =userIds[index];
			if(folderMap[userid])
				continue;
			candidateUserInfo.push({
				userid : userid,
				username : userProfileFile[userid].username,
				postLimit : postLimit,
				cursor : ''
			})
		}
		var saveCount = 0; 
		console.log("total candidate ",candidateUserInfo.length);
		async.each(candidateUserInfo, function(option, eachCB){
			disqus.saveTimeline(option, function(err, body){
				saveCount += 1;
				eachCB(null)
			})
		}, function(error){
			console.log("Intotal saved ",saveCount);
			if(!error)
				res.status(200).json({message: "successfully stored data"});
			else
				res.status(400).json(error);
		})
	},
	'post extractDisqusComments': function(req,res){
		disqus.extractComments({
			limit 	: ((req.body.limit || req.body.limit == 0)? parseInt(req.body.limit) : 500),
			offset 	: ((req.body.offset || req.body.offset == 0)? parseInt(req.body.offset) : 0),
		}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'check': function(req,res){
		var filename = (req.body.filename ? req.body.filename : config.data.disqusUserWithProfiles.base+'/merged/temp_1-200000.json');
			
		var userProfileFile = JSON.parse(fs.readFileSync(filename, 'utf-8'));
		var userIds = Object.keys(userProfileFile);
		confused = [];

		for(var index = 0; userIds[index]; index+=1){
			var userid = userIds[index];
			var twitterProfile = userProfileFile[userid].connectedProfiles.twitter;
			var username = userProfileFile[userid].username.toLowerCase();

			if(twitterProfile && Object.keys(twitterProfile).length > 1){
				/*var hasConfusion = 0; //0 -> no confusion, 1-> username contains, 2-> signup, share
				var actualTwitter = null;
				for(var link in twitterProfile){
					if(link.toLowerCase().indexOf(username)!==-1){
						hasConfusion = 1;
						actualTwitter = {}
						actualTwitter[link] = twitterProfile[link]
					}
					if(link.toLowerCase().indexOf('/signup') !==-1 || link.toLowerCase().indexOf('/share') !==-1){
						hasConfusion = 2;
						delete userProfileFile[userid].connectedProfiles.twitter[link]
					}
				}
				if(hasConfusion == 1){
					confused.push(userProfileFile[userid])
					userProfileFile[userid].connectedProfiles.twitter = actualTwitter;
				}
				else if(hasConfusion == 2){*/
					confused.push(userProfileFile[userid])
				//}
			}
		}
		//fs.writeFileSync(config.data.disqusUserWithProfiles.base+'merged/temp_1-200000.json', JSON.stringify(userProfileFile, null,2));
		res.status(200).json({total: confused.length, data:confused})
	},
	'twitterGrep': function(req, res){
		twitter.grepTweet({
			userid	: (req.query.userid ? req.query.userid : 'kaihendry'),
		}, function(err, body){
			console.log('at endpoint return')
			if(!err)
				res.status(200).json({msg:"done"});
			else
				res.status(400).json(err);
		})
	},
	'post saveTwitterTimelineBatch': function(req,res){
		var offset = ((req.body.offset)? parseInt(req.body.offset) : 0);
		var limit  = ((req.body.limit || req.body.limit == 0)? parseInt(req.body.limit) : 10);
		var filename = (req.body.filename ? req.body.filename : config.data.disqusUserWithProfiles.base+'/merged/1-200000.json');
			
		var userProfileFile = JSON.parse(fs.readFileSync(filename, 'utf-8'));
		var userIds = Object.keys(userProfileFile);
		var candidateUserInfo = [];

		for(var index = offset; index < offset+limit && userIds[index]; index+=1){
			var userid =userIds[index];
			var twitterpath = 
				config.data.media.base+
				userIds[index]+'_'+
				userProfileFile[userid].username+'/'+
				config.twitter.timelineFilename;
			
			if(fs.existsSync(twitterpath) || !userProfileFile[userid].connectedProfiles.twitter || Object.keys(userProfileFile[userid].connectedProfiles.twitter).length > 1)
				continue;

			var twitterLinkToken = Object.keys(userProfileFile[userid].connectedProfiles.twitter)[0].split('/');
			candidateUserInfo.push({
				userid : userid,
				username : twitterLinkToken[twitterLinkToken.length-1]
			})
		}
		var saveCount = 0; 
		console.log("total candidate ",candidateUserInfo.length);
		async.each(candidateUserInfo, function(option, eachCB){
			twitter.saveTimeline(option, function(err, body){
				saveCount += 1;
				eachCB(null)
			})
		}, function(error){
			console.log("Intotal saved ",saveCount);
			if(!error)
				res.status(200).json({message: "successfully stored data"});
			else
				res.status(400).json(error);
		})
	},
	/*'post moveTwitterData': function(req,res){
		var filename = (config.data.disqusUserWithProfiles.base+'/merged/1-200000.json');
		var userProfileFile = JSON.parse(fs.readFileSync(filename, 'utf-8'));

		var twitterIds = fs.readdirSync('./new_data');
		var count = 0;

		for(var i=0; twitterIds[i]; i+=1){
			if(twitterIds[i]){
				var id = twitterIds[i].split('.')[0];
				var extra = twitterIds[i].split('.')[1];

				if(extra !== 'json' || !fs.existsSync(config.data.media.base+id+'_'+userProfileFile[id].username) )
					continue
				else{
					var tweetFileName = config.data.media.base+id+'_'+userProfileFile[id].username+'/'+config.twitter.timelineFilename;
					var tweets = JSON.parse(fs.readFileSync('./new_data/'+twitterIds[i],'utf-8'));
					console.log('writing file for '+ tweetFileName)
					fs.writeFileSync(tweetFileName, JSON.stringify(tweets,null,2));
					count += 1;
				}
			}
		}

		res.status(200).json({message: "successfully stored file "+count});
	}*/
}
