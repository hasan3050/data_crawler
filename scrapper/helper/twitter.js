var async = require('async');
var request = require('request');
var fs = require('fs');
var config = require('../../config');
var utility = require('../../utility');
var cheerio = require('cheerio');

function grepTweet(option, callback){
	var id = (option.userid ? option.userid : 'kaihendry');
	
	var createUrl = "http://greptweet.com/create.php?id="+id;
	var intermediateUrl = "http://greptweet.com/u/"+id+"/";
	var tweetFileUrl = "http://greptweet.com/u/"+id+"/tweets.txt";

	request.get({url:createUrl}, function(createError, createResponse, createBody){
		if (!createError && createResponse.statusCode == 200){
			
			console.log("crated url for "+id+", now going to sleep for 10 second");
			utility.sleep(10);
			
			request.get({url:intermediateUrl}, function(intError, intResponse, intBody){
				
				if (!intError && intResponse.statusCode == 200){
					
					console.log("intermediate url for "+id+", now going to sleep for 20 second");
					utility.sleep(20);
					
					request.get({url:tweetFileUrl,gzip: true}, function(tweetError, tweetResponse, tweetBody){
						console.log("got data for "+id+", now going to return");
					
						if (!tweetError && tweetResponse.statusCode == 200 && tweetBody){
							
							var tweets = tweetBody.split('\n');
							var tweetList = [];
							for (var i = 0; i < tweets.length-1; i++) {
		                        var tweetToken = tweets[i].split('|');
		                        if(/^\d+$/.test(tweetToken[0])===false) {
		                            tweetList[tweetList.length-1].tweet+='\n'+tweetToken[0];
		                            continue;
		                        }
		                        var tweetId = tweetToken[0];
		                        var tweetTime = (tweetToken[2]? tweetToken[1] : null );
		                        var tweet = tweetToken[2] || tweetToken[1];
		                        
		                        var isRetweet = false;

		                        if (tweet && tweet.search('RT @') == 0) {
		                            isRetweet = true;
		                            tweet = tweet.slice(3);
		                        }

		                        tweetList.push({
		                        	tweetId: tweetId,
		                        	isReTweet: isRetweet, 
		                        	time: tweetTime, 
		                        	tweet: tweet
		                        })
		                    }

		                    //fs.writeFileSync(config.data.media.base+'twitter.text', JSON.stringify(tweetList,null,2));
		                    callback(null, tweetList)
						}
						else
							callback(tweetError, null)
					})
				}
				else
					callback(intError, null)
			})
		}
		else
			callback(createError, null)
	})
}

function saveTimeline(option, callback){
	var activities = [];
	var dirPath = config.data.media.base + option.userid+'_'+option.username;
	var filename = config.twitter.timelineFilename;
	async.series([
		function(getTimelineCB){
			grepTweet(option, function(err, body){
				if(!err && body)
					activities = body;
				else
					err = err || "No Data Found"
				getTimelineCB(err)
			})
		},
		function (createDirCB) {
			console.log(dirPath)
			var hasDir = fs.existsSync(dirPath)
			if(!hasDir) {
                utility.directoryCreate(dirPath, 0777, function (err, res) {
                	console.log(err, res)
                    if (!err || err.errno == 34)
                        createDirCB(null);
                    else
                        createDirCB(err);
                });
            }
            else
            	createDirCB(null)
        },
        function (saveTimelineDataCB) {
            console.log('>>>now write data for ' + option.username)
            fs.writeFile(dirPath + '/' + filename, JSON.stringify(activities,null,2), function (err) {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(">>>The file was saved for " + option.username);
                }
                saveTimelineDataCB(err)
            })
        }
	], function(error){
		callback(error, {message: ((!error)? "Data stored succesfully" : "Failed")})
	})
}

module.exports = {
	grepTweet: grepTweet,
	saveTimeline: saveTimeline
}