var async = require('async');
var request = require('request');
var fs = require('fs');
var config = require('../../config');
var utility = require('../../utility');

module.exports = {
	timeline:function(option, callback){
		getTimeline(option, function(err, activities){
			callback(err, activities)
		})
    },
    saveTimeline: function(option, callback){
    	var activities = {};
    	var dirPath = config.data.media.base + option.userid+'_'+option.username;
    	var filename = config.disqus.timelineFilename;
    	async.series([
    		function(getTimelineCB){
    			getTimeline(option, function(err, body){
    				if(!err)
    					activities = body;
					getTimelineCB(err)
				})
    		},
    		function (createDirCB) {
    			console.log(dirPath)
                utility.directoryCreate(dirPath, 0777, function (err, res) {
                	console.log(err, res)
                    if (!err || err.errno == 34)
                        createDirCB(null);
                    else
                        createDirCB(err);
                });
            },
            function (saveTimelineDataCB) {
                console.log('>>>now write data for ' + option.username)
                fs.writeFile(dirPath + '/' + filename, JSON.stringify(activities), function (err) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(">>>The file was saved for " + option.username);
                    }
                    saveTimelineDataCB(err)
                })
            }
    	], function(error){
    		callback(error, {message: ((!error)? "Data stored succesfully" : "Failed")})
    	})
    },
    extractComments : function(option, callback){
        var dirPath = config.data.media.base;
        var userList = option.userList || null;
        var limit = option.limit || 500;
        var offset = option.offset || 0;
        var candidateDir = [];
        var count = 0;

        if(!userList || userList.length <=0 ){
            userList = fs.readdirSync(dirPath)
        }

        for(var index = offset; index < offset+limit && userList[index]; index+=1){
            if( userList[index] == '.DS_Store'){
                limit+=1;
                continue;
            }
            candidateDir.push(userList[index])
        }

        async.each(candidateDir, function(dirName, eachCB){
            if(!dirName){
                eachCB(null)
            }
            else{
                extractComments({
                    dirname: dirPath + dirName, 
                    commentFilename: dirPath + dirName + '/' + config.disqus.commentFilename, 
                    storeProfileInfo: true
                }, function(error){
                    count+=1;
                    eachCB(error)
                })
            }
        }, function(err){
            callback(err,count)
        })
    }
}

var extractComments = function(option, callback){
    var timelineFilename = option.dirname + '/' + config.disqus.timelineFilename;
    var commentFilename = option.commentFilename || (option.dirname + '/' + config.disqus.commentFilename);

    var timelineFile = JSON.parse(fs.readFileSync(timelineFilename, 'utf-8'));

    var commentFile = {
        comments : []
    }

    if (option.storeProfileInfo)
        commentFile.profileInfo = timelineFile.profileInfo
    
    for(var threadKey in timelineFile.comments){
        var comment = {
            threadId    : timelineFile.comments[threadKey].threadId,
            post        : timelineFile.comments[threadKey].posts[0].authorPost.raw_message,
            time        : timelineFile.comments[threadKey].posts[0].authorPost.createdAt,
            id          : timelineFile.comments[threadKey].posts[0].authorPost.id
        }

        if (timelineFile.comments[threadKey].posts[0].parentPost) {
            comment.parentPost      = timelineFile.comments[threadKey].posts[0].parentPost.raw_message,
            comment.parentPostTime  = timelineFile.comments[threadKey].posts[0].parentPost.createdAt,
            comment.parentPostId    = timelineFile.comments[threadKey].posts[0].parentPost.id
        };

        commentFile.comments.push(comment)
    }
    
    console.log('---> writing comment file for ', option.dirname+', total comment --> '+Object.keys(timelineFile.comments).length+ '\n');
    fs.writeFileSync(commentFilename, JSON.stringify(commentFile, null, 2));
    callback(null);
}

var getTimeline = function(option, callback){
	var postLimit = ((option && option.postLimit) ? option.postLimit : 2000 );
	var username = ((option && option.username) ? option.username : '' );
	var cursor = ((option && option.cursor)? option.cursor : '');

	var currentResult = {};
    var activities=[];
    var overallResult={profileInfo:{},dataFetchTime:new Date(),threads:{},comments:{}};

    var baseURL = config.disqus.activityURL;
    var apiKey = config.disqus.apiKey;
    var limit = config.disqus.activityChunkLimit;

    console.log("from getTimeline for user -> ",username);

    async.doWhilst(function(getDisqusTimeline) {

        var url = 
        	baseURL+"\&"+
        	"target=user:username:" + username+ "\&"+
        	"api_key=" + apiKey + "\&"+
        	"cursor=" + cursor + "\&"+
        	"limit=" + limit

        //console.log('calling timeline api for ' + username+' and for cursor ->'+cursor)//,' url ->', url);
        request.get({
                url:url
            },
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    currentResult = JSON.parse(body);

                    for (var i = 0; i < currentResult.response.activities.length; i++)
                    {
                        for (var j = 0; j < currentResult.response.activities[i].items.length; j++) {
                            var object = currentResult.response.activities[i].items[j].object;
                            if(object.indexOf('Post')!==-1) {
                                activities.push(object);

                                var userPost = currentResult.response.objects[object];
                                var parentPost = null;
                                var author = userPost.author;
                                var thread = currentResult.response.objects[userPost.thread]

                                if (userPost.parent_post)
                                    parentPost = currentResult.response.objects[userPost.parent_post]

                                overallResult.threads[userPost.thread] = thread;
                                if (author.username == username)
                                    overallResult.profileInfo = author

                                if (overallResult.comments[userPost.thread]) {
                                    overallResult.comments[userPost.thread].posts.push({
                                    	parentPost: parentPost, 
                                    	authorPost: userPost
                                    });
                                }
                                else {
                                    
                                    overallResult.comments[userPost.thread] = {
                                    	threadId: thread.id, 
                                    	posts: [{
                                    		parentPost: parentPost, 
                                    		authorPost: userPost
                                    	}]
                                    }
                                }
                            }
                        }
                    }

                    //console.log('got response for ' + username+' and for cursor '+cursor,'\nError: '+error,'\nPost count: '+activities.length)
                    getDisqusTimeline(null);
                }
                else {
                    getDisqusTimeline({error: 'Access Denied or private profile!', dataFetchTime: new Date()});
                }
            }
        );
    },
    //condition checking function
    function(){
        cursor=currentResult.cursor.next;
        console.log(cursor,(currentResult.cursor.hasNext && activities.length<=postLimit));
        return (currentResult.cursor.hasNext && activities.length<=postLimit);
    },
    function(err){
        if(err){
            console.log(err)
            if(activities.length>0)
                callback(null,overallResult);
            else
                callback(err,null);
        }
        else {
            console.log('Returning profile data for user -> '+username+'\n----------------------------------------\n')
            callback(null, overallResult);
        }
    })
}