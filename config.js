module.exports = {
	"server": {
		"port" : 8090
	},
	"cassandra" : {
		"host" : "localhost",
		"port" : 9042,
		"keyspace" : {
			"basic":"kspc1"
		}
	},
	"elasticsearch" : {
		"host" : "localhost",
		"port" : 9200
	},
	"disqus" : {
		"apiKey" : "E8Uh5l5fHZ6gD8U3KycjAIAk46f68Zw7C6eW8WSjZvCLXebZ7p0r1yrYDrLilk2F",
		"activityURL": "https://disqus.com/api/3.0/timelines/activities?type=profile", //'https://disqus.com/api/3.0/timelines/activities?type=profile\&target=user:username:' + username + '\&cursor=' + cursor + '\&limit=100\&api_key=' + apiKey;
		"detailURL": "https://disqus.com/api/3.0/users/details", // 'https://disqus.com/api/3.0/users/details?user='+id+'&api_key='+apiKey
		"activityChunkLimit":100,
		"timelineFilename":"disqus_timeline.json",
		"commentFilename":"disqus_comments.json"
	},
	"twitter":{
		"timelineFilename":"twitter_timeline.json"
	},
	"data" : {
		"disqusUserWithURL" : {
			"base": "./data/disqusUserWithURL/",
			"error": "error.json"
		},
		"disqusUserWithProfiles" : {
			"base": "./data/disqusUserWithProfiles/"
		},
		"media" : {
			"base": "./data/media/"
		}
	},
	"profileMap" : {
		"g+" 		: /^(?:https?:\/\/)?(?:www\.)?(?:plus\.google\.com\/)(?:u\/0\/)?(?:\++\w+.*|\d+\d|\/?)$/,
		"facebook" 	: /^(?:https?:\/\/)?(?:www\.)?facebook\.com\/(?:\d+|[\w\.]+)\/?$/,
		"twitter"	: /(?:https?:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?@?([\w\-]*)\/?$/,
		"instagram"	: /^(?:https?:\/\/)?(?:www\.)?(?:instagram\.com|instagr\.am)\/([\w\-\.]*)\/?$/,
		"linkedin"	: /^(?:https?:\/\/)?(?:(?:www|\w\w)\.)?linkedin.com\/(?:(?:in\/[^\/]+\/?)|(?:pub\/[^\/]+\/(?:(?:\w|\d)+\/?){3}))$/,
		"stackoverflow" : /^(?:https?:\/\/)?(?:www\.)?(?:stackoverflow\.com\/users\/)(?:\d+\/)(?:[\w\-\.]*)\/?$/,
		"youtube"	: /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/(?:c(?:hannel)?\/|user\/)([\w\-\.]*)\/?$/
	}
}