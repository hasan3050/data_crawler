### Data Crawler Module

This module is used to crawl data from `Disqus`. To run this module use following commands: 
```
    npm start (server will run at default port 8090)
    PORT=<given_port> npm start (for running the app at a specified port)
```
### Data Format
* User profile info:
    * Stored at `{config.data.disqusUserWithProfiles}/chunked` (for individual chunk) and `{config.data.disqusUserWithProfiles}/merged` (for all merged chunk)
    * array of <`key`, `value`> pair, where `key` is the `DISQUS id` and `value` is as follow: 
    ```
        {
            "id": "18",
            "numPosts": 392,
            "numFollowers": 2,
            "rep": 1.388723,
            "numFollowing": 0,
            "location": "Nieuwegein, the Netherlands",
            "isPrivate": false,
            "joinedAt": "2007-08-01T12:54:41",
            "username": "reinout",
            "numLikesReceived": 48,
            "about": "",
            "name": "Reinout van Rees",
            "url": "http://reinout.vanrees.org/",
            "connectedProfiles": {
                "stackoverflow": {
                    "http://stackoverflow.com/users/27401/reinout-van-rees": "stackoverflow"
                },
                "linkedin": {
                    "http://www.linkedin.com/in/reinoutvanrees": "LinkedIn"
                },
                "twitter": {
                    "http://twitter.com/reinoutvanrees": "Twitter"
                }
                
            }
        }
     ```
    * in the `connectedProfiles` property of the `value`, all possible connected social media profile links are enlisted. Only available sites are added under this `connectedProfiles` property. Currently the scrapper checks following seven sites:
        * g+ 
        * twitter
        * facebook
        * linkedin
        * instagram
        * youtube
        * stackoverflow
        
#### API Endpoints:
    - Base URL : http://localhost:PORT

### Scrapper: 
* Get DISQUS timeline : `GET {Base URL}/scrapper/disqusTimeline?username='reinout'&postLimit=100&cursor='1a3cdf20-9d32-11e1-a216-00007ba25615:1:0'`
  * `username` -> username of the desired timeline
  * `postLimit` -> number of posts you want to fetch, default 2000
  * `cursor` -> ofset of DISQUS post, default is `''`

* Save DISQUS timeline : `POST {Base URL}/scrapper/saveDisqusTimeline`
  * `{config.data.media.base}/<userid><_><username>/{config.disqus.timelineFilename}` ; where `config.data.media.base` = `./data/media/` & `config.disqus.timelineFilename` = `disqus_timeline.json`
  * Post parameters: 
      * `userid` -> userid of the profile
      * `username` -> username of the profile
      * `postLimit` -> number of posts you want to fetch, default 2000
      * `cursor` -> ofset of DISQUS post, default is `''`

* Save multiple DISQUS timeline in a batch : `POST {Base URL}/scrapper/saveDisqusTimelineBatch`
  * Read a json file where keys are `DISQUS id` having `DISQUS username` in properties. Saves each timeline at `{config.data.media.base}/<userid><_><username>/{config.disqus.timelineFilename}` ; where `config.data.media.base` = `./data/media/` & `config.disqus.timelineFilename` = `disqus_timeline.json`
  * Post parameters: 
      * `offset` -> offset of the id's. for example if offset is set to 10, then it will omit 1st 9 userid and start from 10th user
      * `limit` -> number of profiles to grab from the offset, default 100 
      * `postLimit` -> number of posts you want to fetch, default 2000
      * `filename` -> file path name, default is `{config.data.disqusUserWithProfiles.base}/merged/1-200000.json`

* Get Twitter timeline : `GET {Base URL}/scrapper/twitterGrep?userid='kaihendry'`

* Save multiple Twitter profile in a batch : `POST {Base URL}/scrapper/saveTwitterTimelineBatch`
  * Read a json file where keys are `DISQUS id` having `DISQUS username` and `connectedProfiles.twitter` in properties. `{config.data.media.base}/<userid><_><username>/{config.twitter.timelineFilename}` ; where `config.data.media.base` = `./data/media/` & `config.disqus.timelineFilename` = `twitter_timeline.json`
  * Post parameters: 
      * `offset` -> offset of the id's. for example if offset is set to 10, then it will omit 1st 9 userid and start from 10th user
      * `limit` -> number of profiles to grab from the offset, default 100 
      * `filename` -> file path name, default is `{config.data.disqusUserWithProfiles.base}/merged/1-200000.json`
