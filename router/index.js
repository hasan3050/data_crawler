var express = require('express');
var router = express.Router();

/*router.use('/disqus', require('./disqus'));
router.use('/scrapper', require('./scrapper'));*/

router.use('/disqus', getRoutes('disqus'));
router.use('/scrapper', getRoutes('scrapper'));

function getRoutes(filename){
	var _router = express.Router();
	var _routes = require('../'+filename+'/route');
	for(var _route in _routes){
		var method = _route.split(' ')[0];
		if(method === 'post'){
			var actualRoute = '/'+_route.split(' ')[1];
			_router.post(actualRoute, _routes[_route]);
		}
		else
			_router.get('/'+_route, _routes[_route]);
	}
	return _router;
}

module.exports = router;