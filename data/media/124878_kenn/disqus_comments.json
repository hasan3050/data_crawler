{
  "comments": [
    {
      "threadId": "4819598515",
      "post": "Isn't it best as soon as we detect a loop, raise a different error earlier than SystemStackError? If people can't be careful enough to follow destroy chain, it sounds like a disaster as shown in your last example waiting to happen. In other words, this feature pleases nobody. Instead we can use the same technique to detect the loop earlier in development or testing, and advice the developer.",
      "time": "2016-05-13T18:22:44",
      "id": "2674155744"
    },
    {
      "threadId": "4784114584",
      "post": "It sounds like JWT is not suitable for some use cases where replay attack needs to be prevented. You can't tamper the header or payload, but you can capture and re-post the same valid content again and again, which could be a problem for non-idempotent APIs like posting an additive score (or item gain) in a game, or check-ins.",
      "time": "2016-05-13T18:06:19",
      "id": "2674126712"
    },
    {
      "threadId": "4712291432",
      "post": "IMO defining specific IPs as MySQL users has scaling problem when you have many app servers to set up / tear down. My preferred method is bind to a private IP, then \"apt-get install ufw; ufw limit 3306/tcp\" which enables rate-limiting on login attempts that is at most 6 attempts per 30 seconds = 2,880 attempts per day, only from IPs in the same datacenter. If you set strong enough passwords that should be fine. That's zero maintenance.",
      "time": "2016-05-03T17:38:36",
      "id": "2655978184"
    },
    {
      "threadId": "4742498219",
      "post": "I thought --skip-lock-tables was redundant when you're using --single-transaction? Is there any reason why you add that option?",
      "time": "2016-05-03T17:30:12",
      "id": "2655963252"
    },
    {
      "threadId": "4622568042",
      "post": "fallback_location: root_path looks too verbose considering how often we're going to use it, don't you think?",
      "time": "2016-03-07T00:16:00",
      "id": "2555277913"
    },
    {
      "threadId": "4505896695",
      "post": "MGM should at least offer a parking receipt that's effective at all of their properties during the same day. Tourists go out and visit other resorts on the strip than where they stay, so free parking for hotel guests is a shortsighted concept.\n\n\nWhat's worse than paying to park is feel the pain multiple times on the same day.",
      "time": "2016-01-20T00:06:30",
      "id": "2467306347"
    },
    {
      "threadId": "4356059561",
      "post": "I think we just saw the same problem with Sidekiq, which calls \"Thread#terminate\" on all worker threads when it receives an interrupt on the main thread. https://github.com/kenn/redis-mutex/issues/22\n\nGreat find, Julia!",
      "time": "2015-12-10T12:08:32",
      "id": "2402950529"
    },
    {
      "threadId": "4213132030",
      "post": "Good point. Thanks for clarification!",
      "time": "2015-11-19T22:59:45",
      "id": "2369061457",
      "parentPost": "That looks great, too! It's a different focus than BulkInsert, though, as it requires you to instantiate the models first, and I'm specifically trying to avoid that. If you can get away with just shoving a bunch of data directly into the database, large imports will be much faster. If, on the other hand, you absolutely must have the validations and callbacks that are configured on your models, the approach I've chosen won't work, and that's by design.",
      "parentPostTime": "2015-11-19T21:55:51",
      "parentPostId": "2368968506"
    },
    {
      "threadId": "4244128962",
      "post": "UUID also kills the locality of reference when used with a clustered index like InnoDB. Normally the \"hot spot\" that is accessed often tends to be in the most recent data, which will be concentrated in specific blocks on the disk and cached in-memory when you're with an auto-increment primary key. What you're killing is B-tree on-disk layout - it is a major drawback at scale. Also the key length is longer - 4 bytes for 32-bit int or 8 bytes for 64-bit int vs 16 bytes for UUID. All in all, the base overhead is much higher with UUID.\n\nIn theory, complete randomness is rather a concept of noise that masks signals. UUID spreads values evenly in the given address space. It has a better place for cryptography, but not so compatible with indexing. It's made intentionally hard to be found.\n\nThere are other approaches for distributed primary keys - like offsetting (server A starts with 1,000,000,000,001 and server B starts with 10,000,000,000,001, etc.) which is actually used by Apple's in-app purchase system, so let's not just depend on \"love\" to architecture such a critical part of the entire system. :)",
      "time": "2015-11-02T13:47:50",
      "id": "2338893751",
      "parentPost": "I worked on a project once that used UUIDs as the primary key. I agree that *technically* there's no reason not to use them. But damn it is the most frustrating thing in the world when trying to debug issues with specific records. It's one of those cases where theory doesn't take into account the actual humans who have to deal with them. \n\n\nUsually you can turn to someone and ask \"what user?\" and get \"3053\" and go look them up. Not with UUIDs—you need to email or IM every ID to a coworker. You also can't remember foreign keys to try and look up that user's blog posts, comments, etc. You need to copy/paste IDs and foreign keys EVERY. SINGLE. TIME. For me the technical benefits weren't worth the hassle of actually maintaining the app on a daily basis.\n\n\nI suppose once you get to millions of records in a table then the IDs become long enough that you can't remember them anyway, in which case you may as well have UUIDs. But for most of us it'll be several years (if ever) before we get to that point. And you'll be much happier those first few years when you can remember a couple of IDs in your head!",
      "parentPostTime": "2015-10-22T17:01:15",
      "parentPostId": "2321181254"
    },
    {
      "threadId": "4205775682",
      "post": "Did you take a look at bootstrap-4.0.0.alpha at https://rails-assets.org ?",
      "time": "2015-10-16T09:50:57",
      "id": "2310173484"
    },
    {
      "threadId": "3135135747",
      "post": "CPU is one of the most expensive resources, so I always pay the most attention to it. https://gist.github.com/kenn/5228906\n\nFor larger projects with 100+ servers, a cluster of 4GB app servers + a replication group of large database instances is my favorite. As a rule, smaller instances offer better price-per-core - $10/core in 1G - 4G range, but starts to degrade from there - a 96GB instance costs you $48/core.\n\nFor smaller projects, I like to have the full-stack in one server and a replicated hot standby for redundancy, so whatever fits everything. Resource utilization is maximized when web (bandwidth intensive), app (CPU intensive) and DB (disk intensive) run in the same instance. Also, less servers to babysit means better MTBF. On Linode and DigitalOcean, 16GB instance looks like the sweet spot at this moment.",
      "time": "2015-10-16T07:13:14",
      "id": "2310047011",
      "parentPost": "Hi!\n\n\nSince my first approach to hosting for customers has been Linode, I'm starting to look for other options because, you know, get the better from the market.\n\n\nIn your experience with Linode, which plan has been the most useful for your needs? (I have only used the most basic plan having only one core and 1Gb in RAM). How do you measure what you need in power based on requirements (work load, processing power, DBs).\n\n\nThanks!",
      "parentPostTime": "2015-10-16T01:10:11",
      "parentPostId": "2309733410"
    },
    {
      "threadId": "4151452664",
      "post": "has_secure_token now uses Base58 instead of hex in your example, which assumes the database collation to be case sensitive. In other words, it won't work as advertised on MySQL by default...\n\nhttps://github.com/rails/rails/issues/20133",
      "time": "2015-10-03T08:16:44",
      "id": "2287558708"
    },
    {
      "threadId": "4145658379",
      "post": "To me, using default_scope yields more maintainable code than explicit scoping when it's used for soft delete. Forgetting to add the explicit scope leads to disastrous outcome - deleted users showing up in some views, which should never happen. It's a \"safe by default\" matter.\n\nFor soft delete, always use `unscope` on all belongs_to associations as I described above, and that's pretty much it.\n\nFor ordering though, I agree that using default_scope for it is horrible, because it doesn't play nice with complex queries that includes multiple joins.",
      "time": "2015-09-26T15:34:02",
      "id": "2275356553",
      "parentPost": "Thanks for pointing out that alternate syntax. It's definitely better, but still not good, though. The problem with default scopes is not that they can't be worked around, but that they are ultimately unmaintainable.",
      "parentPostTime": "2015-09-26T15:04:10",
      "parentPostId": "2275319427"
    },
    {
      "threadId": "3953963124",
      "post": "I agree that default_scope has some gotchas (I also don't like initialization sets the default value), but the power it brings outweighs those downsides IMO.\n\nFirstly, instead of `Animal.unscoped`, you can do `unscope(where: :deleted_on)` to specifically negate the default scope. You can even define a new scope just for that.\n\nscope :unscope_deleted_on, -> { unscope(where: :deleted_on) }\n\nYou can also do:\n\nbelongs_to :animal, -> { unscope_deleted_on }\n\nfor associations.\n\n\n\nNow, you might reconsider that using default_scope is a good idea. :)",
      "time": "2015-07-31T07:05:40",
      "id": "2166692505"
    },
    {
      "threadId": "3869507469",
      "post": "I wrote a simple gem just for that. https://github.com/kenn/slavery - and I agree with Ken, I'd recommend to use the same application for reporting as well, unless you have a strong reason to do otherwise.",
      "time": "2015-06-29T05:24:38",
      "id": "2106005915",
      "parentPost": "Let's say we have an rails application and a postgresql database. Also, we have another db server for slave replication of our main postgresql database. And I want to use that slave replication as a report engine. Another rails app will work on that, just for reports generating. is it possible ?",
      "parentPostTime": "2015-06-26T13:04:11",
      "parentPostId": "2101289507"
    },
    {
      "threadId": "3853194426",
      "post": "Your point on polymorphic partials sounds really interesting - I've never thought of such a use case, but that makes sense! Probably I should try your avenue myself.\n\nIndeed, I'm glad that we have options to experiment in different directions - those experiments are crucial to find the best practice(s).\n\n\n\nThanks again!",
      "time": "2015-06-24T01:05:30",
      "id": "2095763345",
      "parentPost": "If/when I want helpers available in controllers, I make that specific on a per-helper basis with `helper_method` and I define them in the controller or a superclass.\n\n\nThere are situations (such as polymorphic partials) that benefit from methods that are semantically equivalent having identical names, and, in general, the smaller the surface area of an API, the easier the codebase is to grok.\n\n\nI don't think we are going to agree on this one, but I am glad we have the option to both have it our way!",
      "parentPostTime": "2015-06-23T11:53:04",
      "parentPostId": "2094458039"
    },
    {
      "threadId": "3834410185",
      "post": "The link sent me to a sign up page but I already have an account, and it doesn't seem to work with the existing account. I'm confused.",
      "time": "2015-06-10T01:28:13",
      "id": "2071075706"
    },
    {
      "threadId": "3602150576",
      "post": "Justin, if you use memcache, as you aptly said, sessions and your cached data will be fighting for space, and some unexpected spike in cache usage can cause premature session expiration for online users, which leads to a bad user experience (e.g. suddenly logged out at the last step of purchase). Also, you need to carefully assess the memory cap as your service grow and have more concurrent sessions. Do you have something special to optimize that, or just have plenty of memory and don't worry?\n\nCookieStore is the only way to sync with the browser state lifecycle - when the user quit the browser, the session ends, or as long as the user keeps the tab open, the session continues. No other methods can guarantee that. I think that's another plus for CookieStore.",
      "time": "2015-03-20T12:00:23",
      "id": "1917919301"
    },
    {
      "threadId": "3416404124",
      "post": "Strong consistency is achievable with modern RDBMS like Postgres or MySQL (InnoDB) and I do realize Rails do a great job utilizing those features like atomic UPDATEs on counter increment / decrement. But if you lock a parent record in a transaction while INSERTing or DELETEing a child record, that kills concurrency and that's when a trade-off between performance and consistency pops up. I would say, for mid-sized projects, strong consistency with transaction and parent-locking would work better for less maintenance. For large-scale projects, you are already familiar with the concept of \"eventual-consistency\" and your approach makes more sense. :)",
      "time": "2015-01-23T01:59:29",
      "id": "1812160843"
    },
    {
      "threadId": "3329027608",
      "post": "There were too many monkey patches to list them all here, but it was a long ago. This SO answer has a good set of links to the discussion we had around that time. \n\nhttp://stackoverflow.com/questions/5852626/handling-namespace-models-classes-in-namespace\n\nI believe many in the core team (including DHH) are still not a fan of namespaced models. Probably it's safer to use now, but ActiveRecord is a complicated beast and I don't expect third party libraries to be compliant 100% all the time.\n\nBy the way, you should 1. always use config.eager_load_paths instead of config.autoload_paths (copy-on-write optimization for Unicorn / Passenger + avoid runtime failure), 2. don't need to add app/models. Go check yourself \"Rails.configuration.eager_load_paths\" to see what's in there.\n\nhttp://blog.arkency.com/2014/11/dont-forget-about-eager-load-when-extending-autoload/",
      "time": "2014-12-22T02:55:57",
      "id": "1753927073",
      "parentPost": "just out of curiosity - which \"weird bugs\" did you stumble upon? I basically use namespacing because it helps a lot in readability. The configuration is plain simple:\n\nconfig.autoload_paths += Dir[Rails.root.join('app', 'models'), \"#{config.root}/lib\", \"#{config.root}/lib/validators\"]\n\nnot sure why one does not want to have namespaced Class or Module names when placing them in a subfolder. I mean it's the way you should do it in plain Ruby. As I said - I am curious ;-)",
      "parentPostTime": "2014-12-21T14:34:25",
      "parentPostId": "1752822598"
    },
    {
      "threadId": "2787136301",
      "post": "Hi Josh. Good article - your conclusion is consistent with mine - Linode wins on the performance front, but it is a close call.\n\nBut, if you have a system at a massive scale (100+ servers) only Linode offers the feature set that is required for such use cases (IP hot swap for failover, physical host distribution, etc.) however DO is catching up.\n\nLesser variance = predictable performance is another front that Linode wins, as shown in your result, I have the data for SSD. https://gist.github.com/kenn/51af188d35dac8069313\n\nAnd finally, I'm also interested in the test if 4x 512 DO outperforms 1x 2G Linode. I suppose it's about the CPU, and I've done some calculation about that. https://gist.github.com/kenn/5228906 the important factor is the \"price per core\" as application servers are stateless / shared-nothing and horizontally scalable. You can put 2x Rails processes per 512 DO and utilize 100% CPU even when there's 50% iowait (e.g. database) on average. That is 4 cores total, whereas Linode 2G only offers 2 cores.\n\nI was even happier when Linode offered 8x cores on all instances at some point in the past - I was able to run a massive array of 2GB instances to saturate all 8 cores each! Yeah it felt like I was gaming the system so now I'm happy that Linode distributes the available cores in line with the plan structure. The current pricing structure looks pretty solid to the eyes of those who don't leave any resources underutilized. :)",
      "time": "2014-11-27T07:25:03",
      "id": "1714951349"
    },
    {
      "threadId": "3241100132",
      "post": "I know it works beautifully, but that's not really intuitive as it requires you know how Dirty works internally, like when you initialize a new object, does that count as \"changed\"?, etc. I would prefer `if: -> { new_record? || password.present? }` but it may be only me.",
      "time": "2014-11-22T22:27:18",
      "id": "1707686715",
      "parentPost": "You don't need a subclass for that! This works fine:\n\nvalidates :password, length: { minimum: 9 }, if: :password_changed?",
      "parentPostTime": "2014-11-19T22:34:51",
      "parentPostId": "1702188463"
    },
    {
      "threadId": "3141076286",
      "post": "Great post. Would like to hear what you think about https://github.com/kenn/enum_accessor too.",
      "time": "2014-10-28T07:20:13",
      "id": "1657129330"
    },
    {
      "threadId": "2999258513",
      "post": "もうちょっと9th側だったらよかったのにねぇ",
      "time": "2014-09-09T04:17:28",
      "id": "1580439232"
    },
    {
      "threadId": "3843599072",
      "post": "Service objects are a good first step, but as you put everything but the kitchen sink, it could get complicated very quickly. I have multiple directories that contain POROs - services, decorators, forms, facades, serializers, jobs, redis, etc. Good thing is that those folders are auto-loaded / eager-loaded with zero configuration. I would put something under app/services only when I couldn't find a better place. :)",
      "time": "2014-09-05T01:50:02",
      "id": "1574731487"
    },
    {
      "threadId": "2943846169",
      "post": "That will break in more complex queries when the `includes` turn into LEFT OUTER JOIN  ( http://guides.rubyonrails.org/active_record_querying.html#specifying-conditions-on-eager-loaded-associations ) but yeah, it would work, as we don't usually use `pluck` in scope methods.\n\n\nBTW for that reason I never use strings in scope methods - `where(arel_table[:updated_at].gt 2.days.ago)` rather than `where('updated_at > ?', 2.days.ago)` to maintain composability.",
      "time": "2014-08-31T19:43:02",
      "id": "1567770845",
      "parentPost": "You can .pluck(\"employees.last_name\") to avoid ambiguities.",
      "parentPostTime": "2014-08-30T22:16:50",
      "parentPostId": "1566811810"
    },
    {
      "threadId": "2961134143",
      "post": "Now I have the canonical reference link when I review codes. Thanks Avdi!",
      "time": "2014-08-28T17:24:50",
      "id": "1563584298"
    },
    {
      "threadId": "2926044265",
      "post": "Right. What PgBouncer does essentially is a *very* coarse (session / transaction / statement) mutex on pooled connections. It works well when most of connections are idling, but it doesn't at scale with higher QPS.",
      "time": "2014-08-26T22:00:35",
      "id": "1560729585",
      "parentPost": "Depending on the nature of the workload, PgBouncer can reduce the number of actual Postgres connections needed without affecting how clients interact with the server (e.g. persistent connections, etc).\n\n\n\nMost of that 10MiB is consumed by temp_buffers, so again depending on workload one could reduce that to good effect.",
      "parentPostTime": "2014-08-26T21:08:00",
      "parentPostId": "1560651731"
    },
    {
      "threadId": "2845469946",
      "post": "I stopped using `redirect_to ... and return if ...` at some point, since then I use `return redirect_to ... if ...` instead, because a controller action method does not care about the return value.\n\nThat way, there's no `and` mental overhead, plus it's clearer to see `return` at the beginning of a line that it's intended as early return, in the same way that the new RSpec 3 has `expect` at the beginning of a line. It goes well with `if` modifiers. Plus, it's 4 less characters per line. :)",
      "time": "2014-07-17T23:26:58",
      "id": "1490574262"
    },
    {
      "threadId": "2790707242",
      "post": "All good points! I guess this topic alone warrants a new blog post, or a gem? :) There seems to be many decisions and trade-offs to make. I'll try to follow your advice and use AnonymousUser with ID:1 next time. Thanks again!",
      "time": "2014-07-08T05:35:53",
      "id": "1473711623",
      "parentPost": "It depends. I'll usually make it a PORO that mixes in ActiveModel::Model. The API I have to reimplement is usually small enough that it's not too big of a deal, and I don't have to worry about dealing with undoing the persistence part of ActiveRecord::Base. \n\n\nIf User has complicated logic, I'll sometimes extract that to a module and mix it into both, or just inherit from User. But if you inherit, you have to override some methods to not allow persistence, and I always worry that I'll miss some.\n\n\nIt can actually be useful to have your null object actually persisted to the database -- for example, AnonymousUser could just be the user with ID 1. This can get a little confusing and you'll need some special-casing to get everything to work correctly, but then you get to use a lot more of the built-in Rails functionality. I've been doing this less often since ActiveModel came around, though.",
      "parentPostTime": "2014-07-07T16:37:04",
      "parentPostId": "1472257840"
    },
    {
      "threadId": "2571213481",
      "post": "Your view always makes me smile. :) HyperLogLog has been one of the most interesting ideas to me lately. One thing I'm still wondering is that it depends on the side-characteristics of a hash function - and which hash function turns out to be the best for HyperLogLog. As with any CSPRNGs, we'll never be 100% confident about them.",
      "time": "2014-04-04T15:47:45",
      "id": "1318522537"
    },
    {
      "threadId": "2337177536",
      "post": "That's great info, thanks! Is there any article that explains the details? I'm interested in how that is done.",
      "time": "2014-02-21T17:46:11",
      "id": "1255082200",
      "parentPost": "The difference there isn't skipping the partial lookup (which is cached), it's an optimization performed when a collection is given that only evals the result once, instead of 1000 times. (String eval is slow)",
      "parentPostTime": "2014-02-16T15:46:52",
      "parentPostId": "1246847079"
    },
    {
      "threadId": "2094346237",
      "post": "言及ありがとうございます。\n\nおっしゃることはよくわかります。ぼくも昔はエンタープライズなソフトウェア業界でプロマネやってましたので、そちら方面に伝わるような内容では一切ないという自覚はあります。なのでたびたび「ウェブアプリ開発」って限定して書いてたんですけど、「スタートアップの」とか「小規模の自社ウェブアプリで」ぐらいまで絞っておいたほうが良かったかもしれませんね。\n\nスタートアップでは、ふとした会話でこの変更やってみようとなったら、その場でコード書き始めてすぐ動くものができて、テストを走らせてオールグリーンで、そのままstagingへデプロイして、同僚にみてもらって、よければ本番に出す、というサイクルが全体で10分とかだったりします。それでバグが出たらすぐ通知がくるので、すぐなおしてリリースするのに追加で3分とか。そういうスピード感とかサイクルでやってる感覚が理解されないと、ぼくの言ってることはたぶん全く意味不明だと思います。\n\nでも、ぼくは小学生低学年のころから続けてソフトウェア開発歴30年を超えてますので、日米両国でいろいろな業界をみてきてますし、テストエンジニアを否定しているつもりは毛頭なくて、最後に「ウェブアプリ開発が1980年代のミドルウェア開発やゲーム開発などと同じ道のりをたどっていくとするならば、この次にくるのは専業のQAチームが開発チームと独立して存在してリリースのサインオフ権限を握り、想定外のパターンをテストしまくってリリース前にバグを出し切る、という体制になるのかもしれない。」とも言及していてます。\n\nぼくが強く主張しているのは、むしろ「開発者自身が書くテストの価値なんてたかが知れている」です。むろん開発者だって様々な例外ケースを考えてコード書きますが、80%の品質は20%の時間で達成できるので、それを100%に近づけようとすると5倍の時間がかかるので、必ずトレードオフが存在します。そして、主にスタートアップにおけるエンジニアは実装スピードで評価されるので、そのトレードオフは前者に倒れがちです。スタートアップに求められる実装のスピード感に必要なメンタリティと、異常系をねちねち意地悪く洗うときのメンタリティは相互に矛盾します。だから「開発者自身が書くテスト」なんて、マスターベーションと揶揄しましたがほんとに役に立たないものを書きがちで、テストの実行がどんどん遅くなっていくだけのゴミみたいなのがどんどん積もっていくんですね。（本当にゴミで、1+2=3をテストしてるようなトートロジーだらけです）\n\nこれを放置して溜まってしまうと、テストの実行が遅すぎるためにテストの実行を手元でやらなくなり、それを回避するためにJenkinsとかCIまかせになり、つまりテストがあるのに問題検出に何十分もかかるとかいう世界になり、10分で開発から本番リリースまでというサイクルが維持できなくなります。かといっていまさらテストコードを掃除する時間もとれなくてプロジェクト自体がドロガメのように遅くなっていきます。\n\nただ、規模が大きくなってくれば、もちろんドロガメのように遅くても仕組みとして回ることがpay offする損益分岐点は存在します。そして、QAエンジニアは「開発者本人とは違う視点でテスト」ができることそれ自体に大きな意義があると思っています。\n\nだから、ぼくが述べたことは一般化できるものではありません。一応、誤解を解きたくてコメントしました。",
      "time": "2014-01-11T18:11:09",
      "id": "1196464537"
    },
    {
      "threadId": "1949683768",
      "post": "Not working on Safari either.",
      "time": "2013-11-23T00:50:40",
      "id": "1135216986"
    },
    {
      "threadId": "1720822466",
      "post": "mailtrap.io良いですね。開発中の見た目のチェックに限っていえば https://github.com/ryanb/letter_opener とか https://github.com/37signals/mail_view も便利ですよね。",
      "time": "2013-10-22T07:45:22",
      "id": "1091829706"
    },
    {
      "threadId": "1443648458",
      "post": "All your points are valid in theory, but practically MyISAM is not a viable option for such a serious use case, with the durability concern alone as the original article suggests.\n\nAlso, given that kind of structure, we're actually seeing 20,000qps on InnoDB even without saturating CPU cores, meaning the InnoDB overhead if at all is negligible that I'm not sure what you're trying to achieve by eliminating it. Can't see the wood for the trees.",
      "time": "2013-06-28T16:56:22",
      "id": "945492966",
      "parentPost": "The problem with InnoDB (well more than one problem really, in this case) is that InnoDB locks are complex structures which take more time to acquire and release than MyISAM locks, so the overhead is even higher than just the concurrency reduction due to serialization.  And each update writes UNDO and REDO which are expensive operations on a small hot table in InnoDB.  Any op is much more expensive than the same op in MyISAM because more data has to be read and written to maintain MVCC.  InnoDB just has data structures to merge writes and reduce overall overhead in many ways, but if you take away the ability to do that it falls over.",
      "parentPostTime": "2013-06-28T16:26:12",
      "parentPostId": "945452577"
    },
    {
      "threadId": "1180424860",
      "post": "Congrats, you guys have come through... thanks for keeping personal use for free! I think it's a great way to make potentially large audience as a prospect in the long term.",
      "time": "2013-04-03T02:42:30",
      "id": "850205993"
    },
    {
      "threadId": "1109414372",
      "post": "How faster is it going to be? Is it worth upgrade when I have the Roku 2 XS?",
      "time": "2013-03-06T05:45:33",
      "id": "820727409"
    },
    {
      "threadId": "972787684",
      "post": "When we use JRuby, how many concurrency do we have per dyno? Still throttled at the reverse proxy?",
      "time": "2012-12-14T06:40:26",
      "id": "735690175"
    },
    {
      "threadId": "955329891",
      "post": "チャーシューに味がない（塩味足りない）のが残念な感じだったね。見た目とのギャップが結構ある。。。",
      "time": "2012-12-06T08:27:43",
      "id": "728995226",
      "parentPost": "うーん、Ken Ken Ramenはあまり美味しくなかった記憶。でもゆず塩は試す価値ありそうですね。今度また行ってみますー。\nちなみにKen Ken Ramen自体は結構前からあったかと。",
      "parentPostTime": "2012-12-03T06:07:51",
      "parentPostId": "725858882"
    },
    {
      "threadId": "923944112",
      "post": "Excellent post! I'm impressed.\n\nIf you're using Postgres, an array-type column with GIN-index holding foreign keys would perform better, no?",
      "time": "2012-11-16T22:18:05",
      "id": "712223336"
    },
    {
      "threadId": "866226366",
      "post": "Yeah, that's the correct way to do it. But sometimes there are environments (not you) that do not behave like that (e.g. thin), and since there's no way to remove at_exit hooks, you end up using exit! or add a hook \"at_exit { exit! }\" at the top for failsafe.",
      "time": "2012-06-05T20:26:10",
      "id": "548166103",
      "parentPost": "I tend to write exit handlers so they don't inherit.  I just pull the PID when I define it, then inside the hook's block I wrap the code in an if check with the current PID.  I only take action if they match.",
      "parentPostTime": "2012-06-05T19:36:46",
      "parentPostId": "548125220"
    },
    {
      "threadId": "705808003",
      "post": "Am I seeing a different chat app on your MBA screen? :p",
      "time": "2012-05-29T02:14:40",
      "id": "540673778",
      "parentPost": "We use Hipchat at work, so its iOS app does it job, although it's not a great iOS app. For IRC chat I use Limechat.",
      "parentPostTime": "2012-05-28T18:11:48",
      "parentPostId": "540455182"
    },
    {
      "threadId": "679939693",
      "post": "Good write-up.\n\nIf a pure-ruby parser of moped is faster than bson_ext, it usually means something is dead wrong with bson_ext, or the benchmark is done wrong (didn't test on large data, etc.). It should have been order of magnitude faster.\n\nTime to consider ffi?\n\nhttps://github.com/ffi/ffi/wiki/Why-use-FFI",
      "time": "2012-05-28T16:05:19",
      "id": "540386829"
    },
    {
      "threadId": "692966342",
      "post": "まだ全体の数字が小さいのでなんともですが、used_memory_humanとused_memory_rssの比がmem_fragmentation_ratioで、1.98ってことは結構freeされにくい小さいオブジェクトが多数ある感じでしょうか。経験則では、STRINGよりHASHを多用してkeyよりvalueのサイズが大きくなるよう設計したほうがfrag_ratioについてはおおむねいい結果（効率的なメモリ利用）が得られるようです（KEYSコマンド使いたい局面ものちのち出てくるでしょうし、そうするとキーの総エントリ数を小さく抑えるのが大事になってきます）。\n\nあとはマシン性能にもよりますが50,000/qpsに近づくと危険水域なのと（CPU 1個しか使えない）、used_memory_peakが大事ですかね、これが大きくなりすぎるとswapや最悪でoomのリスクが出てきますから。。。数GB未満のデータセットをsnapshot persistenceだけで運用するならこのぐらいですかね、運用設計が楽なのもredisのいいところだと思います。",
      "time": "2012-05-25T16:06:15",
      "id": "538485124"
    },
    {
      "threadId": "698215189",
      "post": "味についての言及が一切ないのねw",
      "time": "2012-05-21T14:06:47",
      "id": "534417818"
    },
    {
      "threadId": "427964621",
      "post": "Great post, but there's one thing to note - you should fall back to the local copy in case the CDN is down.\n\nI created a gem that takes care of it for you, hope it's useful! https://github.com/kenn/jquery-rails-cdn",
      "time": "2012-04-22T03:35:55",
      "id": "505916311"
    },
    {
      "threadId": "601336071",
      "post": "Congrats, Mitchell!\n\nIf it weren't for Vagrant, it would be impossible to create a new provisioning utility on my own - Sunzi. ( https://github.com/kenn/sunzi ) Vagrant let me start from a clean state again and again, which was exactly what I needed.\n\nVagrant is in use for devs at http://pankia.com/ as well, which is pretty successful.\n\nThank you!",
      "time": "2012-03-09T04:48:18",
      "id": "460338793"
    },
    {
      "threadId": "459946037",
      "post": "Or more in general, is there any way to conditionally run a bulk of commands in a task, based on the current role the commands are being run on? I often feel frustrated that I have to define many redundant tasks with a very little diff, just to separate roles. Not really DRY.",
      "time": "2012-02-10T21:58:48",
      "id": "435632925"
    },
    {
      "threadId": "436784171",
      "post": "It could be OpenCL/GPGPU thing now that's 7x faster on A5. Still need an answer before passing judgement.",
      "time": "2011-10-07T17:16:03",
      "id": "328976374",
      "parentPost": "It is very unlikely that Siri can work with the new CPU but can't with the old one. The single core of the old CPU should be more or less as fast as a single core of the A5. I don't think Siri is using a multi threading implementation saturating the CPU during the recognition. We'll see however...\n\nAlso there are already dictation programs for iPhone 4 working well enough... this is all evidence that it is unlikely that the iPhone 4 was not albe to run Siri, but we'll have more details soon.\n\nThat could be a good reason for not including it in the iPhone 4 btw.",
      "parentPostTime": "2011-10-07T15:02:57",
      "parentPostId": "328888315"
    },
    {
      "threadId": "115319174",
      "post": "If you are using Nginx, it's probably because you haven't set config.action_dispatch.x_sendfile_header = \"X-Accel-Redirect\" in production.rb. I had the same problem and solved that way.\nhttp://stackoverflow.com/questions/3724853/rails-sends-0-byte-files-using-send-file/3727122#3727122",
      "time": "2011-02-18T17:46:34",
      "id": "151487606",
      "parentPost": "This reply might be more than a little late, but I ran into the same problem today and managed to solve it.\n\nBasically the solution is to copy the static assets from the Resque gem (use 'bundle show resque' to figure out where it is) to your public directory so that your web server can serve them directly. Alternatively you could symlink the files to your app's public directory in production.\n\nThe static assets are in lib/resque/server/public/ in the resque gem directory.",
      "parentPostTime": "2011-02-06T11:51:11",
      "parentPostId": "141543311"
    }
  ],
  "profileInfo": {
    "username": "kenn",
    "about": "",
    "name": "Kenn Ejima",
    "disable3rdPartyTrackers": false,
    "isPowerContributor": false,
    "isAnonymous": false,
    "rep": 1.2664199999999999,
    "profileUrl": "https://disqus.com/by/kenn/",
    "url": "http://twitter.com/kenn",
    "reputation": 1.2664199999999999,
    "location": "",
    "isPrivate": false,
    "signedUrl": "http://disq.us/?url=http%3A%2F%2Ftwitter.com%2Fkenn&key=mTlYaK23KCCsBirTQEVk8w",
    "isPrimary": true,
    "joinedAt": "2008-10-20T16:00:32",
    "id": "124878",
    "avatar": {
      "small": {
        "permalink": "https://disqus.com/api/users/avatars/kenn.jpg",
        "cache": "//a.disquscdn.com/uploads/users/12/4878/avatar32.jpg?1463163765"
      },
      "isCustom": true,
      "permalink": "https://disqus.com/api/users/avatars/kenn.jpg",
      "cache": "//a.disquscdn.com/uploads/users/12/4878/avatar92.jpg?1463163765",
      "large": {
        "permalink": "https://disqus.com/api/users/avatars/kenn.jpg",
        "cache": "//a.disquscdn.com/uploads/users/12/4878/avatar92.jpg?1463163765"
      }
    }
  }
}