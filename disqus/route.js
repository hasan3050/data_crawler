//var client = require('../connection').cassandraClient;
var user = require('./helper/user');
var website = require('./helper/website');
var async = require('async');

module.exports ={
	'user': function(req,res){
		user.detail({
			offset: ((req.query.offset)? parseInt(req.query.offset) : 1),
			limit : ((req.query.limit || req.query.limit == 0)? parseInt(req.query.limit) : 100)
		}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'post crawlUser': function(req,res){
		user.crawlDetail({
			offset: ((req.body.offset)? parseInt(req.body.offset) : 1),
			limit : ((req.body.limit || req.body.limit == 0)? parseInt(req.body.limit) : 100),
			chunkSize : ((req.body.chunkSize)? parseInt(req.body.chunkSize) : 1000)
		}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'post assembleUser' : function(req, res){
		user.assembleUserWithURL({
			offset: ((req.body.offset)? parseInt(req.body.offset) : 1),
			limit : ((req.body.limit || req.body.limit == 0)? parseInt(req.body.limit) : 100000),
			chunkSize : ((req.body.chunkSize)? parseInt(req.body.chunkSize) : 1000)
		} , function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'website' : function(req, res){
		website.scrap({
			url: req.query.url
		} , function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'allWebsite' : function(req, res){
		website.scrapAll({
			fileName: req.query.fileName,
			limit : (req.query.limit ? parseInt(req.query.limit) : 10),
			offset: ((req.query.offset)? parseInt(req.query.offset) : 1)
		} , function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'post assemble' : function(req, res){
		website.assemble( {}, function(err, body){
			if(!err)
				res.status(200).json(body);
			else
				res.status(400).json(err);
		})
	},
	'allWebsiteTemp' : function(req, res){
		var chunkList = [];
		var offset= ((req.query.offset)? parseInt(req.query.offset) : 0);
		var limit = (req.query.limit ? parseInt(req.query.limit) : 50);
		var lastIndex = (req.query.lastIndex? parseInt(req.query.lastIndex):3364);
		
		for(var i=offset; i< lastIndex; i+=limit ) {
			chunkList.push({
				fileName : req.query.fileName,
				limit : ((i+limit > lastIndex) ? (lastIndex-i-limit) : limit),
				offset : i
			})
		}

		async.eachSeries(chunkList, function(chunk, callback){
			website.scrapAll(chunk , function(err, body){
				if(!err)
					callback(null)
				else
					callback({error: err, chunk: chunk})
			})
		}, function(error){
			if(!error)
				res.status(200).json({});
			else
				res.status(400).json(error);
		})
	}
}
