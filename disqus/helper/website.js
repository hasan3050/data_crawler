var cheerio = require('cheerio');
var async = require ('async');
var request = require('request');
var fs=require('fs');
var config = require('../../config');


module.exports = {
	'scrap' : function(option, callback){
		scrapLinks(option, function(links){
			callback(null,links)
		})
	},
	'assemble' : function(option, callback){
		assemble(option, function(error, body){
			callback(error,body)
		})
	},
	'scrapAll' : function(option, callback){
		
		var fileName = (option && option.fileName)? option.fileName : (config.data.disqusUserWithURL.base+'merged/100001-200000.json');
		var profileMap = JSON.parse(fs.readFileSync(fileName, 'utf-8'));
		var limit = (option && option.limit) ? option.limit : 10;
		var offset = (option && option.offset) ? option.offset : 1;
		var profileList = [];
		var i = 1;
		var updatedProfileMap = {};

		for( var id in profileMap ) {
			if( i >= offset )
				profileList.push(profileMap[id]);
			
			i += 1;
			
			if( i >= offset + limit -1 )
				break;
		}

		async.each(profileList, function( profile , eachCB){
		
			scrapLinks({
				
				url : profile.url,
				id  : profile.id,
				username: profile.username
			
			}, function( connectedProfiles){
				if( connectedProfiles && Object.keys(connectedProfiles).length > 0 ) {
					
					//console.log(profile.id, profile.username , ' ----> ', JSON.stringify(connectedProfiles, null, 2));
					profile.connectedProfiles = connectedProfiles;
					updatedProfileMap[profile.id] = profile;
				}

				console.log(profile.id, profile.username, profile.url)

				eachCB(null);
			})
		
		}, function(error){
			console.log('all scraping is done, now storing file');
			var ids = profileList;
			var idKey = ids[0].id+'-'+ids[ids.length-1].id;
			var baseFilePath = config.data.disqusUserWithProfiles.base;
			var fileName = baseFilePath+'chunked/'+idKey+'.json';
			console.log(fileName, "count -> ", Object.keys(updatedProfileMap).length, " offset -> ", offset);
			fs.writeFileSync(fileName, JSON.stringify(updatedProfileMap, null,2));
			callback(error, updatedProfileMap)
		
		})
	}
}

var scrapLinks = function(option, callback){
	//console.log("Now started for ", option.id, option.username, option.url)
	var url = (option && option.url)? option.url : "http://www.unqualified.org/";
	var profileMap = (option && option.profileMap) ? option.profileMap : config.profileMap;

	request.get({

		url: url,
		timeout: (60*1000)
	
	}, function(error, response, body){
		
		var profile = {};

		if(!error && (response.statusCode == 200) && body){
			$ = cheerio.load(body);
			
			var links = $('body').find('a');
			var len = links.length

			for( var i = 0 ; i<len; i+=1 ){
				
				var href = $(links[i]).attr('href') || $(links[i]).attr('data-href');

				var profileType = checkProfile(href, profileMap)
                		
        		if( profileType && !profile[profileType])
        			profile[profileType] = {};

        		if(profileType)
        			profile[profileType][href] = ( $(links[i]).text() ? $(links[i]).text().trim() : '' );
			}
		}

		var profileType = checkProfile(url, profileMap)
                		
		if( profileType && !profile[profileType])
			profile[profileType] = {};

		if(profileType)
			profile[profileType][url] = url;

		callback(profile)
	}).setMaxListeners(0);
}

var assemble = function(option, callback){
	var userInfo = {}
	var path = config.data.disqusUserWithProfiles.base+'/chunked';
	var fileNames = fs.readdirSync(path);

	for(var i=0; fileNames[i]; i+=1){
		console.log(fileNames[i]);
		
		var file = JSON.parse(fs.readFileSync(path+'/'+ fileNames[i], 'utf-8'));
		
		for(var id in file)
			userInfo[id] = file[id]
	}

	fs.writeFileSync(config.data.disqusUserWithProfiles.base+'merged/100001-200000.json', JSON.stringify(userInfo, null,2));

	callback(null, Object.keys(userInfo).length);
}

var checkProfile = function ( url , profileMap) {
	profileMap = profileMap || config.profileMap;

	for(var profileType in profileMap){
    	
    	if( url && url.match(profileMap[profileType]) ) {

    		return profileType;
    	}
	}

	return null;

}