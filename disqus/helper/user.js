//var client = require('../../connection').cassandraClient;
var async = require ('async');
var request = require('request');
var fs=require('fs');
var config = require('../../config');

module.exports = {
	detail : function(option, callback){
		getDetail(option, function(err, res){
			callback(err, res)
		})
	},
	crawlDetail : function(option, callback){
		var baseFilePath = config.data.disqusUserWithURL.base;
		var errorFileName = baseFilePath + config.data.disqusUserWithURL.error;

		var limit = ((option && option.limit) ? option.limit : 100 );
		var offset = ((option && option.offset) ? option.offset : 1 );
		var chunkSize = ((option && option.chunkSize)? option.chunkSize : 1000);

		var options = [];
		for(var i=offset; i-offset<limit; i+=chunkSize){
			options.push({
				offset 	: i,
				limit	: Math.min(chunkSize, limit+offset-i )
			})
		}

		async.each(options, function(option, eachCB){
			getDetail(option, function(detailErr, userWithURL){
				var idKey = parseInt(option.offset) + '-' + parseInt(option.offset+option.limit-1);
				
				if(detailErr || !userWithURL){
					var errorReport=fs.existsSync(errorFileName)?JSON.parse(fs.readFileSync(errorFileName,'utf-8')):{};
					
					errorReport[idKey] = "Failed to get data.\nerror ["+ JSON.stringify(detailErr)+"]";
					fs.writeFileSync(errorFileName,JSON.stringify(errorReport, null, 2));
					eachCB(null);
				}
				else{
					var fileName = baseFilePath+'chunked/'+idKey+'.json';
					fs.writeFileSync(fileName, JSON.stringify(userWithURL, null,2));
					eachCB(null);
				}
			})
		}, function(err){
			callback(err)
		})
	},
	assembleUserWithURL : function(option, callback){
		assembleUserWithURL(option, function(error, userCount){
			callback(error, {
				userCount : userCount
			})
		})
	}
}

var getDetail = function (option, callback) {
	var ids = [];
	var userWithURL = {};
	var limit = ((option && (option.limit || option.limit == 0)) ? option.limit : 100 )
	var offset = ((option && option.offset) ? option.offset : 1 )
	
	for(var i=offset; i-offset<limit; i+=1){
		ids.push(i);
	}

	async.each(ids, function(id, eachCB){
		var endpoint = 
			config.disqus.detailURL + 
			'?api_key='+config.disqus.apiKey+
			'&user='+id;

		request.get({
            url:endpoint
        },
        function (error, response, body) {
        	
        	if (!error && body && response.statusCode == 200) {
        		body = ((typeof body == 'object')? body : JSON.parse(body).response);
        	
        		if(body.url && (body.isPrivate == false) && body.numPosts >= 50) {
        			console.log(body.id, body.username);
        			userWithURL[body.id] = {
						"id": body.id,
						"numPosts": body.numPosts,
						"numFollowers": body.numFollowers,
						"rep": body.rep,
						"numFollowing": body.numFollowing,
						"location": body.location,
					    "isPrivate": body.isPrivate,
					    "joinedAt": body.joinedAt,
					    "username": body.username,
					    "numLikesReceived": body.numLikesReceived,
					    "about": body.about,
					    "name": body.name,
					    "url": body.url,
					}
        		}
        	}	
            eachCB(error)
        })
	}, function(err){
		callback(err, userWithURL)
	})
}

var assembleUserWithURL = function(option, callback){
	var limit = ((option && option.limit) ? option.limit : 100000 );
	var offset = ((option && option.offset) ? option.offset : 1 );
	var chunkSize = ((option && option.chunkSize)? option.chunkSize : 1000);

	var userInfo = {};
	var fileNames = [];
	var margedFileName = (offset+'-'+(offset+limit-1)+'.json');

	for(var i=offset; i-offset<limit; i+=chunkSize)
		fileNames.push(i+'-'+(i+chunkSize-1)+'.json')

	for(var i=0; fileNames[i]; i+=1){
		if(fileNames[i] == config.data.disqusUserWithURL.error)
			continue;
		else{
			var file = JSON.parse(fs.readFileSync(config.data.disqusUserWithURL.base+'chunked/'+ fileNames[i], 'utf-8'));
			
			for(var id in file)
				userInfo[id] = file[id]
		}
	}

	fs.writeFileSync(config.data.disqusUserWithURL.base+'merged/'+margedFileName, JSON.stringify(userInfo, null,2));
					

	callback(null, Object.keys(userInfo).length);
}