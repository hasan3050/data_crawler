var fs= require('fs');
var path = require('path');

fs.mkdirParent = function(dirPath, mode, callback) {
    //Call the standard fs.mkdir
    fs.mkdir(dirPath, mode, function(error) {
        //When it fail in this way, do the custom steps
        if (error && error.errno === 34) {
            //Create all the parents recursively
            fs.mkdirParent(path.dirname(dirPath), mode, callback);
            //And then the directory
            fs.mkdirParent(dirPath, mode, callback);

            //Manually run the callback since we used our own callback to do all these
            callback && callback(error);
        }
        else 
            callback(null)
    });
};

module.exports = {
	toCqlDateTime : function(date){
	    date = date || new Date();
	    date = 
	    	date.getFullYear() + '-' +
	        ('00' + (date.getMonth()+1)).slice(-2) + '-' +
	        ('00' + date.getDate()).slice(-2) + ' ' +
	        ('00' + date.getHours()).slice(-2) + ':' +
	        ('00' + date.getMinutes()).slice(-2) + ':' +
	        ('00' + date.getSeconds()).slice(-2);

    	return date;
	},
	directoryCreate : fs.mkdirParent,
    sleep: function(s) {
      var e = new Date().getTime() + (s * 1000);

      while (new Date().getTime() <= e) {
        ;
      }
    }
}